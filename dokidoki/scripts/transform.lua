local v2 = require 'dokidoki.v2'

pos = pos or v2(0, 0)
facing = facing or v2(1, 0)
scale_x = scale_x or 1
scale_y = scale_y or 1
