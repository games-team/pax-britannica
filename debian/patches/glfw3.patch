From: James Cowgill <jcowgill@debian.org>
Date: Wed, 25 Apr 2018 23:08:26 +0200
Subject: Port to GLFW3

This is a very basic port of the dokidoki-support sub-module to GLFW3. I've
left a number of functions which are not used by pax-britannica unimplemented
which simplifies the patch. The new versions of some of these functions do not
match with the current API. The correct way to do this would probably be a full
rewrite of luaglfw.c with a new API, but on the basis that pax-britannica seems
to be dead upstream, I think this is acceptable for the time being.
---
 dokidoki-support/luaglfw.c | 426 ++++++++++++++++++++++++++-------------------
 dokidoki-support/mixer.c   |  31 ++--
 2 files changed, 261 insertions(+), 196 deletions(-)

diff --git a/dokidoki-support/luaglfw.c b/dokidoki-support/luaglfw.c
index 8410e06..5192b6e 100644
--- a/dokidoki-support/luaglfw.c
+++ b/dokidoki-support/luaglfw.c
@@ -29,9 +29,12 @@
 //========================================================================
 
 #include <lauxlib.h>
-#include <GL/glfw.h>
+#include <GLFW/glfw3.h>
+#include <errno.h>
+#include <time.h>
 #include "luaglfw.h"
 
+static GLFWwindow* window;
 
 //************************************************************************
 //****                   Internal type definitions                    ****
@@ -102,19 +105,19 @@ static void pushVideoMode( lua_State *L, const GLFWvidmode *mode)
 {
     lua_newtable( L );
     lua_pushstring( L, "Width" );
-    lua_pushnumber( L, mode->Width );
+    lua_pushnumber( L, mode->width );
     lua_rawset( L, -3 );
     lua_pushstring( L, "Height" );
-    lua_pushnumber( L, mode->Height );
+    lua_pushnumber( L, mode->height );
     lua_rawset( L, -3 );
     lua_pushstring( L, "RedBits" );
-    lua_pushnumber( L, mode->RedBits );
+    lua_pushnumber( L, mode->redBits );
     lua_rawset( L, -3 );
     lua_pushstring( L, "GreenBits" );
-    lua_pushnumber( L, mode->GreenBits );
+    lua_pushnumber( L, mode->greenBits );
     lua_rawset( L, -3 );
     lua_pushstring( L, "BlueBits" );
-    lua_pushnumber( L, mode->BlueBits );
+    lua_pushnumber( L, mode->blueBits );
     lua_rawset( L, -3 );
 }
 
@@ -130,12 +133,11 @@ static const char * windowclose_name;
 static const char * windowrefresh_name;
 static const char * mousebutton_name;
 static const char * mousepos_name;
-static const char * mousewheel_name;
 static const char * key_name;
 static const char * char_name;
 
 
-void GLFWCALL luaglfw_windowsizefun( int w, int h )
+void luaglfw_windowsizefun( GLFWwindow* window_dummy, int w, int h )
 {
     lua_State *L = callback_lua_state;
     if( L == NULL ) return;
@@ -149,23 +151,26 @@ void GLFWCALL luaglfw_windowsizefun( int w, int h )
     }
 }
 
-int GLFWCALL luaglfw_windowclosefun( void )
+void luaglfw_windowclosefun( GLFWwindow* window_dummy )
 {
     lua_State *L = callback_lua_state;
     int do_close = 1;
-    if( L == NULL ) return 1;
 
-    lua_getglobal( L, windowclose_name );
-    if( lua_isfunction( L, -1 ) )
+    if( L != NULL )
     {
-        lua_pcall( L, 0, 1, 0 );
-        do_close = (int) lua_tonumber( L, -1 );
-        lua_pop( L, 1 );
+        lua_getglobal( L, windowclose_name );
+        if( lua_isfunction( L, -1 ) )
+        {
+            lua_pcall( L, 0, 1, 0 );
+            do_close = (int) lua_tonumber( L, -1 );
+            lua_pop( L, 1 );
+        }
     }
-    return do_close;
+
+    glfwSetWindowShouldClose(window, do_close);
 }
 
-void GLFWCALL luaglfw_windowrefreshfun( void )
+void luaglfw_windowrefreshfun( GLFWwindow* window_dummy )
 {
     lua_State *L = callback_lua_state;
     if( L == NULL ) return;
@@ -177,7 +182,7 @@ void GLFWCALL luaglfw_windowrefreshfun( void )
     }
 }
 
-void GLFWCALL luaglfw_mousebuttonfun( int button, int action )
+void luaglfw_mousebuttonfun( GLFWwindow* window_dummy, int button, int action, int mods )
 {
     lua_State *L = callback_lua_state;
     if( L == NULL ) return;
@@ -191,7 +196,7 @@ void GLFWCALL luaglfw_mousebuttonfun( int button, int action )
     }
 }
 
-void GLFWCALL luaglfw_mouseposfun( int x, int y )
+void luaglfw_mouseposfun( GLFWwindow* window_dummy, double x, double y )
 {
     lua_State *L = callback_lua_state;
     if( L == NULL ) return;
@@ -199,29 +204,19 @@ void GLFWCALL luaglfw_mouseposfun( int x, int y )
     lua_getglobal( L, mousepos_name );
     if( lua_isfunction( L, -1 ) )
     {
-        lua_pushnumber( L, (lua_Number)x );
-        lua_pushnumber( L, (lua_Number)y );
+        lua_pushnumber( L, (lua_Number)(int)x );
+        lua_pushnumber( L, (lua_Number)(int)y );
         lua_pcall( L, 2, 0, 0 );
     }
 }
 
-void GLFWCALL luaglfw_mousewheelfun( int wheelpos )
+void luaglfw_keyfun( GLFWwindow* window_dummy, int key, int scancode, int action, int mods )
 {
     lua_State *L = callback_lua_state;
     if( L == NULL ) return;
 
-    lua_getglobal( L, mousewheel_name );
-    if( lua_isfunction( L, -1 ) )
-    {
-        lua_pushnumber( L, (lua_Number)wheelpos );
-        lua_pcall( L, 1, 0, 0 );
-    }
-}
-
-void GLFWCALL luaglfw_keyfun( int key, int action )
-{
-    lua_State *L = callback_lua_state;
-    if( L == NULL ) return;
+    // Filter out GLFW_REPEAT actions
+    if (action == GLFW_REPEAT) return;
 
     lua_getglobal( L, key_name );
     if( lua_isfunction( L, -1 ) )
@@ -232,7 +227,7 @@ void GLFWCALL luaglfw_keyfun( int key, int action )
     }
 }
 
-void GLFWCALL luaglfw_charfun( int key, int action )
+void luaglfw_charfun( GLFWwindow* window_dummy, unsigned int key )
 {
     lua_State *L = callback_lua_state;
     if( L == NULL ) return;
@@ -241,7 +236,7 @@ void GLFWCALL luaglfw_charfun( int key, int action )
     if( lua_isfunction( L, -1 ) )
     {
         lua_pushnumber( L, (lua_Number)key );
-        lua_pushnumber( L, (lua_Number)action );
+        lua_pushnumber( L, (lua_Number)GLFW_PRESS );
         lua_pcall( L, 2, 0, 0 );
     }
 }
@@ -269,6 +264,7 @@ static int glfw_Init( lua_State *L )
 static int glfw_Terminate( lua_State *L )
 {
     glfwTerminate();
+    window = NULL;
     return 0;
 }
 
@@ -287,14 +283,20 @@ static int glfw_GetVersion( lua_State *L )
 // Window handling
 //========================================================================
 
+#define GLFW2_WINDOW               0x00010001
+#define GLFW2_FULLSCREEN           0x00010002
+
 static int glfw_OpenWindow( lua_State *L )
 {
     lua_Number  w, h, r, g, b, a, depth, stencil, mode;
-    int         argc, res;
+    int         res;
 
     // Check arguments
     if( badArgs( L, 9, "OpenWindow" ) ) return 0;
 
+    // Disallow if a window is already open
+    if (window != NULL) return 0;
+
     // Get all arguments to glfwOpenWindow
     w       = lua_tonumber( L, 1 );
     h       = lua_tonumber( L, 2 );
@@ -306,11 +308,21 @@ static int glfw_OpenWindow( lua_State *L )
     stencil = lua_tonumber( L, 8 );
     mode    = lua_tonumber( L, 9 );
 
-    // Call glfwOpenWindow
+    // Call glfwCreateWindow
     lua_settop( L,0 );
-    res = glfwOpenWindow( (int)w, (int)h, (int)r, (int)g, (int)b,
-                          (int)a, (int)depth, (int)stencil,
-                          (int)mode );
+    glfwWindowHint(GLFW_RED_BITS, (int)r);
+    glfwWindowHint(GLFW_GREEN_BITS, (int)g);
+    glfwWindowHint(GLFW_BLUE_BITS, (int)b);
+    glfwWindowHint(GLFW_ALPHA_BITS, (int)a);
+    glfwWindowHint(GLFW_DEPTH_BITS, (int)depth);
+    glfwWindowHint(GLFW_STENCIL_BITS, (int)stencil);
+
+    GLFWmonitor* monitor = (mode == GLFW2_FULLSCREEN) ? glfwGetPrimaryMonitor() : NULL;
+    window = glfwCreateWindow((int) w, (int)h, "Pax Britannica", monitor, NULL);
+    res = (window == NULL) ? GL_FALSE : GL_TRUE;
+
+    if (window != NULL)
+        glfwMakeContextCurrent(window);
 
     // Return result
     lua_pushnumber( L, (lua_Number)res );
@@ -324,13 +336,18 @@ static int glfw_OpenWindowHint( lua_State *L )
     target = lua_tonumber( L, 1 );
     hint   = lua_tonumber( L, 2 );
     lua_settop( L, 0 );
-    glfwOpenWindowHint( (int)target, (int)hint );
+    glfwWindowHint( (int)target, (int)hint );
     return 0;
 }
 
 static int glfw_CloseWindow( lua_State *L )
 {
-    glfwCloseWindow();
+    if (window != NULL)
+    {
+        glfwDestroyWindow(window);
+        window = NULL;
+    }
+
     return 0;
 }
 
@@ -338,9 +355,13 @@ static int glfw_SetWindowTitle( lua_State *L )
 {
     const char *str;
     if( badArgs( L, 1, "SetWindowTitle" ) ) return 0;
+
+    // Disallow if a window is not open
+    if (window == NULL) return 0;
+
     str = lua_tostring( L, 1 );
     lua_remove( L, 1 );
-    glfwSetWindowTitle( str );
+    glfwSetWindowTitle(window, str);
     return 0;
 }
 
@@ -348,17 +369,25 @@ static int glfw_SetWindowSize( lua_State *L )
 {
     lua_Number w, h;
     if( badArgs( L, 2, "SetWindowSize" ) ) return 0;
+
+    // Disallow if a window is not open
+    if (window == NULL) return 0;
+
     w = lua_tonumber( L, 1 );
     h = lua_tonumber( L, 2 );
     lua_settop( L, 0 );
-    glfwSetWindowSize( (int)w, (int)h );
+    glfwSetWindowSize(window, (int)w, (int)h);
     return 0;
 }
 
 static int glfw_GetWindowSize( lua_State *L )
 {
     int w, h;
-    glfwGetWindowSize( &w, &h );
+
+    // Disallow if a window is not open
+    if (window == NULL) return 0;
+
+    glfwGetWindowSize(window, &w, &h);
     lua_settop( L, 0 );
     lua_pushnumber( L, (lua_Number)w );
     lua_pushnumber( L, (lua_Number)h );
@@ -369,32 +398,63 @@ static int glfw_SetWindowPos( lua_State *L )
 {
     lua_Number x, y;
     if( badArgs( L, 2, "SetWindowPos" ) ) return 0;
+
+    // Disallow if a window is not open
+    if (window == NULL) return 0;
+
     x = lua_tonumber( L, 1 );
     y = lua_tonumber( L, 2 );
     lua_settop( L, 0 );
-    glfwSetWindowPos( (int)x, (int)y );
+    glfwSetWindowPos(window, (int)x, (int)y);
     return 0;
 }
 
 static int glfw_IconifyWindow( lua_State *L )
 {
-    glfwIconifyWindow();
+    // Disallow if a window is not open
+    if (window == NULL) return 0;
+
+    glfwIconifyWindow(window);
     return 0;
 }
 
 static int glfw_RestoreWindow( lua_State *L )
 {
-    glfwRestoreWindow();
+    // Disallow if a window is not open
+    if (window == NULL) return 0;
+
+    glfwRestoreWindow(window);
     return 0;
 }
 
+#define GLFW2_OPENED               0x00020001
+#define GLFW2_ACCELERATED          0x00020004
+
 static int glfw_GetWindowParam( lua_State *L )
 {
     lua_Number n;
     if( badArgs( L, 1, "GetWindowParam" ) ) return 0;
+
+    // Disallow if a window is not open
+    if (window == NULL) return 0;
+
     n = lua_tonumber( L, 1 );
     lua_settop( L, 0 );
-    n = glfwGetWindowParam( (int)n );
+
+    // Handle special cases
+    switch ((int) n)
+    {
+        case GLFW2_OPENED:
+            n = !glfwWindowShouldClose(window);
+            break;
+        case GLFW2_ACCELERATED:
+            n = GL_TRUE;
+            break;
+        default:
+            n = glfwGetWindowAttrib(window, (int)n);
+            break;
+    }
+
     lua_pushnumber( L, n );
     return 1;
 }
@@ -407,7 +467,11 @@ static int glfw_GetWindowParam( lua_State *L )
 
 static int glfw_SwapBuffers( lua_State *L )
 {
-    glfwSwapBuffers();
+    // Disallow if a window is not open
+    if (window == NULL) return 0;
+
+    glfwSwapBuffers(window);
+    glfwPollEvents();
     return 0;
 }
 
@@ -426,14 +490,10 @@ static int glfw_SwapInterval( lua_State *L )
 // Video modes
 //========================================================================
 
-#define LUAGLFW_MAX_NUM_MODES 256
-
 static int glfw_GetVideoModes( lua_State *L )
 {
-    GLFWvidmode modes[ LUAGLFW_MAX_NUM_MODES ];
     int modecount, i;
-
-    modecount = glfwGetVideoModes( modes, LUAGLFW_MAX_NUM_MODES );
+    const GLFWvidmode* modes = glfwGetVideoModes(glfwGetPrimaryMonitor(), &modecount);
 
     lua_newtable( L );
     for( i = 0; i < modecount; i++ )
@@ -447,10 +507,7 @@ static int glfw_GetVideoModes( lua_State *L )
 
 static int glfw_GetDesktopMode( lua_State *L )
 {
-    GLFWvidmode mode;
-    glfwGetDesktopMode( &mode );
-
-    pushVideoMode( L, &mode );
+    pushVideoMode( L, glfwGetVideoMode(glfwGetPrimaryMonitor()) );
 
     return 1;
 }
@@ -483,6 +540,9 @@ static int glfw_GetKey( lua_State *L )
 
     if( badArgs( L, 1, "GetKey" ) ) return 0;
 
+    // Disallow if a window is not open
+    if (window == NULL) return 0;
+
     switch( lua_type( L, 1 ) )
     {
         case LUA_TSTRING:
@@ -495,7 +555,7 @@ static int glfw_GetKey( lua_State *L )
     }
 
     lua_settop( L, 0 );
-    n = glfwGetKey( n );
+    n = glfwGetKey(window, n);
     lua_pushnumber( L, n );
     return 1;
 }
@@ -504,16 +564,24 @@ static int glfw_GetMouseButton( lua_State *L )
 {
     lua_Number n;
     if( badArgs( L, 1, "GetMouseButton" ) ) return 0;
+
+    // Disallow if a window is not open
+    if (window == NULL) return 0;
+
     n = lua_tonumber( L, 1 );
     lua_settop( L, 0 );
-    lua_pushnumber( L, glfwGetMouseButton( (int)n ) );
+    lua_pushnumber( L, glfwGetMouseButton(window, (int)n) );
     return 1;
 }
 
 static int glfw_GetMousePos( lua_State *L )
 {
-    int x, y;
-    glfwGetMousePos( &x, &y );
+    double x, y;
+
+    // Disallow if a window is not open
+    if (window == NULL) return 0;
+
+    glfwGetCursorPos(window, &x, &y);
     lua_settop( L, 0 );
     lua_pushnumber( L, (lua_Number)x );
     lua_pushnumber( L, (lua_Number)y );
@@ -524,29 +592,26 @@ static int glfw_SetMousePos( lua_State *L )
 {
     lua_Number x, y;
     if( badArgs( L, 2, "SetMousePos" ) ) return 0;
+
+    // Disallow if a window is not open
+    if (window == NULL) return 0;
+
     x = lua_tonumber( L, 1 );
     y = lua_tonumber( L, 2 );
     lua_settop( L, 0 );
-    glfwSetMousePos( (int)x, (int)y );
+    glfwSetCursorPos(window, (double)x, (double)y);
     return 0;
 }
 
 static int glfw_GetMouseWheel( lua_State *L )
 {
-    int pos;
-    pos = glfwGetMouseWheel();
-    lua_settop( L, 0 );
-    lua_pushnumber( L, (lua_Number)pos );
-    return 1;
+    unsupportedFunction(L, "GetMouseWheel");
+    return 0;
 }
 
 static int glfw_SetMouseWheel( lua_State *L )
 {
-    lua_Number pos;
-    if( badArgs( L, 1, "SetMouseWheel" ) ) return 0;
-    pos = lua_tonumber( L, 1 );
-    lua_settop( L, 0 );
-    glfwSetMouseWheel( (int)pos );
+    unsupportedFunction(L, "SetMouseWheel");
     return 0;
 }
 
@@ -555,6 +620,10 @@ static int glfw_SetMouseWheel( lua_State *L )
 // Joystick input
 //========================================================================
 
+#define GLFW2_PRESENT              0x00050001
+#define GLFW2_AXES                 0x00050002
+#define GLFW2_BUTTONS              0x00050003
+
 static int glfw_GetJoystickParam( lua_State *L )
 {
     lua_Number joy, param;
@@ -563,17 +632,29 @@ static int glfw_GetJoystickParam( lua_State *L )
     joy   = lua_tonumber( L, 1 );
     param = lua_tonumber( L, 2 );
     lua_settop( L, 0 );
-    res = glfwGetJoystickParam( (int)joy, (int)param );
+
+    switch((int) param)
+    {
+        case GLFW2_PRESENT:
+            res = glfwJoystickPresent(joy);
+            break;
+        case GLFW2_AXES:
+            glfwGetJoystickAxes(joy, &res);
+            break;
+        case GLFW2_BUTTONS:
+            glfwGetJoystickButtons(joy, &res);
+            break;
+        default:
+            res = 0;
+    }
+
     lua_pushnumber( L, (lua_Number)res );
     return 1;
 }
 
-#define LUAGLFW_MAX_JOY_AXES 256
-
 static int glfw_GetJoystickPos( lua_State *L )
 {
     int   joy, numaxes, res, i;
-    float pos[ LUAGLFW_MAX_JOY_AXES ];
 
     // Get arguments
     if( badArgs( L, 2, "GetJoystickPos" ) ) return 0;
@@ -581,10 +662,10 @@ static int glfw_GetJoystickPos( lua_State *L )
     numaxes = (int)lua_tonumber( L, 2 );
     lua_settop( L, 0 );
     if( numaxes < 1 ) return 0;
-    if( numaxes > LUAGLFW_MAX_JOY_AXES ) numaxes = LUAGLFW_MAX_JOY_AXES;
 
     // Call GLFW funciton
-    res = glfwGetJoystickPos( joy, pos, numaxes );
+    const float* pos = glfwGetJoystickAxes(joy, &res);
+    if (res > numaxes) res = numaxes;
 
     // Create result array
     lua_newtable( L );
@@ -598,12 +679,9 @@ static int glfw_GetJoystickPos( lua_State *L )
     return 1;
 }
 
-#define LUAGLFW_MAX_JOY_BUTTONS 256
-
 static int glfw_GetJoystickButtons( lua_State *L )
 {
     int           joy, numbuttons, res, i;
-    unsigned char buttons[ LUAGLFW_MAX_JOY_AXES ];
 
     // Get arguments
     if( badArgs( L, 2, "GetJoystickButtons" ) ) return 0;
@@ -611,11 +689,10 @@ static int glfw_GetJoystickButtons( lua_State *L )
     numbuttons = (int)lua_tonumber( L, 2 );
     lua_settop( L, 0 );
     if( numbuttons < 1 ) return 0;
-    if( numbuttons > LUAGLFW_MAX_JOY_BUTTONS )
-        numbuttons = LUAGLFW_MAX_JOY_BUTTONS;
 
     // Call GLFW funciton
-    res = glfwGetJoystickButtons( joy, buttons, numbuttons );
+    const unsigned char* buttons = glfwGetJoystickButtons(joy, &res);
+    if (res > numbuttons) res = numbuttons;
 
     // Create result array
     lua_newtable( L );
@@ -653,13 +730,38 @@ static int glfw_SetTime( lua_State *L )
     return 0;
 }
 
+#define ONE_BILLION 1000000000
+
 static int glfw_Sleep( lua_State *L )
 {
-    lua_Number t;
+    // This function is no longer implemented by glfw, but is required for
+    // pax-britannica, so replace it with a nanosleep implementation.
+    double t;
     if ( badArgs( L, 1, "Sleep" ) ) return 0;
-    t = lua_tonumber( L, 1 );
+    t = (double) lua_tonumber( L, 1 );
     lua_settop( L, 0 );
-    glfwSleep( (double)t );
+
+    // Cap negative sleep durations
+    if (t < 0) t = 0;
+
+    // Perform the sleep
+    struct timespec req;
+    req.tv_sec = (time_t) t;
+    req.tv_nsec = (long) ((t - (time_t) t) * ONE_BILLION);
+
+    if (req.tv_nsec >= ONE_BILLION)
+    {
+        req.tv_sec++;
+        req.tv_nsec = 0;
+    }
+    else if (req.tv_nsec < 0)
+    {
+        req.tv_nsec = 0;
+    }
+
+    while (nanosleep(&req, &req) == -1 && errno == EINTR)
+        ;
+
     return 0;
 }
 
@@ -748,11 +850,8 @@ static int glfw_BroadcastCond( lua_State *L )
 
 static int glfw_GetNumberOfProcessors( lua_State *L )
 {
-    int n;
-    n = glfwGetNumberOfProcessors();
-    lua_settop( L,0 );
-    lua_pushnumber( L, (lua_Number)n );
-    return 1;
+    unsupportedFunction( L, "GetNumberOfProcessors" );
+    return 0;
 }
 
 
@@ -762,13 +861,9 @@ static int glfw_GetNumberOfProcessors( lua_State *L )
 
 static int glfw_GetGLVersion( lua_State *L )
 {
-    int major, minor, rev;
-    glfwGetGLVersion( &major, &minor, &rev );
-    lua_settop( L,0 );
-    lua_pushnumber( L, (lua_Number)major );
-    lua_pushnumber( L, (lua_Number)minor );
-    lua_pushnumber( L, (lua_Number)rev );
-    return 3;
+    // GLFW3 requires an open window now, so most uses of this will fail anyway
+    unsupportedFunction( L, "GetGLVersion" );
+    return 0;
 }
 
 static int glfw_ExtensionSupported( lua_State *L )
@@ -790,21 +885,13 @@ static int glfw_ExtensionSupported( lua_State *L )
 
 static int glfw_Enable( lua_State *L )
 {
-    lua_Number param;
-    if ( badArgs( L, 1, "Enable" ) ) return 0;
-    param = lua_tonumber( L, 1 );
-    lua_settop( L, 0 );
-    glfwEnable( (int)param );
+    unsupportedFunction( L, "Enable" );
     return 0;
 }
 
 static int glfw_Disable( lua_State *L )
 {
-    lua_Number param;
-    if ( badArgs( L, 1, "Disable" ) ) return 0;
-    param = lua_tonumber( L, 1 );
-    lua_settop( L, 0 );
-    glfwDisable( (int)param );
+    unsupportedFunction( L, "Disable" );
     return 0;
 }
 
@@ -827,16 +914,8 @@ static int glfw_FreeImage( lua_State *L )
 
 static int glfw_LoadTexture2D( lua_State *L )
 {
-    const char *name;
-    lua_Number flags;
-    int        res;
-    if( badArgs( L, 2, "LoadTexture2D" ) ) return 0;
-    name  = lua_tostring( L, 1 );
-    flags = lua_tonumber( L, 2 );
-    lua_settop( L, 0 );
-    res = glfwLoadTexture2D( name, (int)flags );
-    lua_pushnumber( L, (lua_Number)res );
-    return 1;
+    unsupportedFunction( L, "LoadTexture2D" );
+    return 0;
 }
 
 
@@ -846,97 +925,112 @@ static int glfw_LoadTexture2D( lua_State *L )
 
 static int glfw_SetWindowSizeCallback( lua_State *L )
 {
+    // Disallow if a window is not open
+    if (window == NULL) return 0;
+
     GLFWwindowsizefun fun = NULL;
     if( lua_isstring( L, 1 ) )
     {
         windowsize_name = lua_tostring( L, 1 );
         fun = luaglfw_windowsizefun;
     }
-    glfwSetWindowSizeCallback( fun );
+    glfwSetWindowSizeCallback( window, fun );
     return 0;
 }
 
 static int glfw_SetWindowCloseCallback( lua_State *L )
 {
+    // Disallow if a window is not open
+    if (window == NULL) return 0;
+
     GLFWwindowclosefun fun = NULL;
     if( lua_isstring( L, 1 ) )
     {
         windowclose_name = lua_tostring( L, 1 );
         fun = luaglfw_windowclosefun;
     }
-    glfwSetWindowCloseCallback( fun );
+    glfwSetWindowCloseCallback( window, fun );
     return 0;
 }
 
 static int glfw_SetWindowRefreshCallback( lua_State *L )
 {
+    // Disallow if a window is not open
+    if (window == NULL) return 0;
+
     GLFWwindowrefreshfun fun = NULL;
     if( lua_isstring( L, 1 ) )
     {
         windowrefresh_name = lua_tostring( L, 1 );
         fun = luaglfw_windowrefreshfun;
     }
-    glfwSetWindowRefreshCallback( fun );
+    glfwSetWindowRefreshCallback( window, fun );
     return 0;
 }
 
 static int glfw_SetMouseButtonCallback( lua_State *L )
 {
+    // Disallow if a window is not open
+    if (window == NULL) return 0;
+
     GLFWmousebuttonfun fun = NULL;
     if( lua_isstring( L, 1 ) )
     {
         mousebutton_name = lua_tostring( L, 1 );
         fun = luaglfw_mousebuttonfun;
     }
-    glfwSetMouseButtonCallback( fun );
+    glfwSetMouseButtonCallback( window, fun );
     return 0;
 }
 
 static int glfw_SetMousePosCallback( lua_State *L )
 {
-    GLFWmouseposfun fun = NULL;
+    // Disallow if a window is not open
+    if (window == NULL) return 0;
+
+    GLFWcursorposfun fun = NULL;
     if( lua_isstring( L, 1 ) )
     {
         mousepos_name = lua_tostring( L, 1 );
         fun = luaglfw_mouseposfun;
     }
-    glfwSetMousePosCallback( fun );
+    glfwSetCursorPosCallback( window, fun );
     return 0;
 }
 
 static int glfw_SetMouseWheelCallback( lua_State *L )
 {
-    GLFWmousewheelfun fun = NULL;
-    if( lua_isstring( L, 1 ) )
-    {
-        mousewheel_name = lua_tostring( L, 1 );
-        fun = luaglfw_mousewheelfun;
-    }
-    glfwSetMouseWheelCallback( fun );
+    unsupportedFunction( L, "SetMouseWheelCallback" );
     return 0;
 }
 
 static int glfw_SetKeyCallback( lua_State *L )
 {
+    // Disallow if a window is not open
+    if (window == NULL) return 0;
+
     GLFWkeyfun fun = NULL;
     if( lua_isstring( L, 1 ) )
     {
         key_name = lua_tostring( L, 1 );
         fun = luaglfw_keyfun;
     }
-    glfwSetKeyCallback( fun );
+    glfwSetKeyCallback( window, fun );
     return 0;
 }
 
 static int glfw_SetCharCallback( lua_State *L )
 {
+    // Disallow if a window is not open
+    if (window == NULL) return 0;
+
     GLFWcharfun fun = NULL;
     if( lua_isstring( L, 1 ) )
     {
         char_name = lua_tostring( L, 1 );
         fun = luaglfw_charfun;
     }
-    glfwSetCharCallback( fun );
+    glfwSetCharCallback( window, fun );
     return 0;
 }
 
@@ -964,8 +1058,7 @@ static struct lua_constant glfw_constants[] =
 
     // Keyboard key definitions
     { "KEY_SPACE", GLFW_KEY_SPACE },
-    { "KEY_SPECIAL", GLFW_KEY_SPECIAL },
-    { "KEY_ESC", GLFW_KEY_ESC },
+    { "KEY_ESC", GLFW_KEY_ESCAPE },
     { "KEY_F1", GLFW_KEY_F1 },
     { "KEY_F2", GLFW_KEY_F2 },
     { "KEY_F3", GLFW_KEY_F3 },
@@ -995,19 +1088,19 @@ static struct lua_constant glfw_constants[] =
     { "KEY_DOWN", GLFW_KEY_DOWN },
     { "KEY_LEFT", GLFW_KEY_LEFT },
     { "KEY_RIGHT", GLFW_KEY_RIGHT },
-    { "KEY_LSHIFT", GLFW_KEY_LSHIFT },
-    { "KEY_RSHIFT", GLFW_KEY_RSHIFT },
-    { "KEY_LCTRL", GLFW_KEY_LCTRL },
-    { "KEY_RCTRL", GLFW_KEY_RCTRL },
-    { "KEY_LALT", GLFW_KEY_LALT },
-    { "KEY_RALT", GLFW_KEY_RALT },
+    { "KEY_LSHIFT", GLFW_KEY_LEFT_SHIFT },
+    { "KEY_RSHIFT", GLFW_KEY_RIGHT_SHIFT },
+    { "KEY_LCTRL", GLFW_KEY_LEFT_CONTROL },
+    { "KEY_RCTRL", GLFW_KEY_RIGHT_CONTROL },
+    { "KEY_LALT", GLFW_KEY_LEFT_ALT },
+    { "KEY_RALT", GLFW_KEY_RIGHT_ALT },
     { "KEY_TAB", GLFW_KEY_TAB },
     { "KEY_ENTER", GLFW_KEY_ENTER },
     { "KEY_BACKSPACE", GLFW_KEY_BACKSPACE },
     { "KEY_INSERT", GLFW_KEY_INSERT },
-    { "KEY_DEL", GLFW_KEY_DEL },
-    { "KEY_PAGEUP", GLFW_KEY_PAGEUP },
-    { "KEY_PAGEDOWN", GLFW_KEY_PAGEDOWN },
+    { "KEY_DEL", GLFW_KEY_DELETE },
+    { "KEY_PAGEUP", GLFW_KEY_PAGE_UP },
+    { "KEY_PAGEDOWN", GLFW_KEY_PAGE_DOWN },
     { "KEY_HOME", GLFW_KEY_HOME },
     { "KEY_END", GLFW_KEY_END },
     { "KEY_KP_0", GLFW_KEY_KP_0 },
@@ -1065,14 +1158,14 @@ static struct lua_constant glfw_constants[] =
     { "JOYSTICK_LAST", GLFW_JOYSTICK_LAST },
 
     // glfwOpenWindow modes
-    { "WINDOW", GLFW_WINDOW },
-    { "FULLSCREEN", GLFW_FULLSCREEN },
+    { "WINDOW", GLFW2_WINDOW },
+    { "FULLSCREEN", GLFW2_FULLSCREEN },
 
     // glfwGetWindowParam tokens
-    { "OPENED", GLFW_OPENED },
-    { "ACTIVE", GLFW_ACTIVE },
+    { "OPENED", GLFW2_OPENED },
+    { "ACTIVE", GLFW_FOCUSED },
     { "ICONIFIED", GLFW_ICONIFIED },
-    { "ACCELERATED", GLFW_ACCELERATED },
+    { "ACCELERATED", GLFW2_ACCELERATED },
     { "RED_BITS", GLFW_RED_BITS },
     { "GREEN_BITS", GLFW_GREEN_BITS },
     { "BLUE_BITS", GLFW_BLUE_BITS },
@@ -1089,31 +1182,10 @@ static struct lua_constant glfw_constants[] =
     { "AUX_BUFFERS", GLFW_AUX_BUFFERS },
     { "STEREO", GLFW_STEREO },
 
-    // glfwEnable/glfwDisable tokens
-    { "MOUSE_CURSOR", GLFW_MOUSE_CURSOR },
-    { "STICKY_KEYS", GLFW_STICKY_KEYS },
-    { "STICKY_MOUSE_BUTTONS", GLFW_STICKY_MOUSE_BUTTONS },
-    { "SYSTEM_KEYS", GLFW_SYSTEM_KEYS },
-    { "KEY_REPEAT", GLFW_KEY_REPEAT },
-    { "AUTO_POLL_EVENTS", GLFW_AUTO_POLL_EVENTS },
-
-    // glfwWaitThread wait modes
-    { "WAIT", GLFW_WAIT },
-    { "NOWAIT", GLFW_NOWAIT },
-
     // glfwGetJoystickParam tokens
-    { "PRESENT", GLFW_PRESENT },
-    { "AXES", GLFW_AXES },
-    { "BUTTONS", GLFW_BUTTONS },
-
-    // glfwReadImage/glfwLoadTexture2D flags
-    { "NO_RESCALE_BIT", GLFW_NO_RESCALE_BIT },
-    { "ORIGIN_UL_BIT", GLFW_ORIGIN_UL_BIT },
-    { "BUILD_MIPMAPS_BIT", GLFW_BUILD_MIPMAPS_BIT },
-    { "ALPHA_MAP_BIT", GLFW_ALPHA_MAP_BIT },
-
-    // Time spans longer than this (seconds) are considered to be infinity
-    { "INFINITY", GLFW_INFINITY },
+    { "PRESENT", GLFW2_PRESENT },
+    { "AXES", GLFW2_AXES },
+    { "BUTTONS", GLFW2_BUTTONS },
 
     { NULL, 0 }
 };
diff --git a/dokidoki-support/mixer.c b/dokidoki-support/mixer.c
index 895753c..015cd0a 100644
--- a/dokidoki-support/mixer.c
+++ b/dokidoki-support/mixer.c
@@ -455,33 +455,30 @@ do \
 } while(0)
 
 #include <alsa/asoundlib.h>
-#include <GL/glfw.h>
+#include <stdatomic.h>
+#include <pthread.h>
 
 static snd_pcm_t * sound_device = NULL;
 
-static GLFWthread audio_thread = 0;
-static volatile int running = 0;
-static GLFWmutex running_mutex = NULL;
+static pthread_t audio_thread;
+static volatile atomic_flag running = ATOMIC_FLAG_INIT;
 
-static GLFWCALL void audio_loop(void *data)
+static void* audio_loop(void *data)
 {
     sample_t buffer[BUFFER_SIZE*2];
 
-    glfwLockMutex(running_mutex);
-    while(running)
+    while(atomic_flag_test_and_set(&running))
     {
-        glfwUnlockMutex(running_mutex);
-
         mix_into(buffer, BUFFER_SIZE);
         int err = snd_pcm_writei(sound_device, buffer, BUFFER_SIZE);
         if(err < 0)
             err = snd_pcm_recover(sound_device, err, 0);
 
-        glfwLockMutex(running_mutex);
         if(err < 0)
             break;
     }
-    glfwUnlockMutex(running_mutex);
+
+    return NULL;
 }
 
 static int init()
@@ -498,20 +495,16 @@ static int init()
 
     ALSA_CHECK(snd_pcm_prepare(sound_device));
 
-    CHECK(running_mutex = glfwCreateMutex(),
-          "audio mutex creation failed");
-    running = 1;
-    audio_thread = glfwCreateThread(audio_loop, NULL);
+    atomic_flag_test_and_set(&running);
+    CHECK(pthread_create(&audio_thread, NULL, audio_loop, NULL) == 0, "error creating alsa thread");
 
     return 1;
 }
 
 static int uninit()
 {
-    glfwLockMutex(running_mutex);
-    running = 0;
-    glfwUnlockMutex(running_mutex);
-    glfwWaitThread(audio_thread, GLFW_WAIT);
+    atomic_flag_clear(&running);
+    pthread_join(audio_thread, NULL);
 
     ALSA_CHECK(snd_pcm_close(sound_device));
     sound_device = NULL;
